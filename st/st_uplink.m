% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
% File: ST Uplink Matrix Decomposition
% Author: Aravindh Krishnamoorthy (c) 2020.
% E-Mail: aravindh.krishnamoorthy@fau.de
%
% Revision 1: Initial revision for public upload.
%

function [U,V1,V2] = st_uplink(A1,A2)

[N,M1] = size(A1) ;
[N1,M2] = size(A2) ;

assert (N == N1) ;

% A1
if M1 >= N
    [V1,~] = qr(rot90(A1',2)) ;
    V1 = rot90(V1,2) ;
    V1 = V1(:,(M1-N)+1:end) ;
    U = eye(N) ;
else
    [U,~] = qr(A1) ;
    U = U' ;
    V1 = eye(M1) ;
end
% A2
[V2,~] = qr(rot90((U*A2)',2)) ;
V2 = rot90(V2,2) ;
if M2 >= N
    V2 = V2(:,(M2-N)+1:end) ;
end
