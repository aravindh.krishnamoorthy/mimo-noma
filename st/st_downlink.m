% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
% File: ST Downlink Matrix Decomposition
% Author: Aravindh Krishnamoorthy (c) 2020.
% E-Mail: aravindh.krishnamoorthy@fau.de
%
% Revision 1: Initial revision for public upload.
%

function [Q1,Q2,P] = st_downlink(H1,H2)

[~,N] = size(H1) ;
[~,NA2] = size(H2) ;
assert (N == NA2) ;

N1 = null(H1) ;
N2 = null(H2) ;
K = null([N1 N2]') ;

% Precoder matrices
P = [K N1 N2] ;
P1 = [K N2] ;
P2 = [K N1] ;

% First receive and precoder matrices
[Q1,~] = qr(H1*P1) ;
Q1 = Q1' ;
% Second receive matrix
[Q2,~] = qr(H2*P2) ;
Q2 = Q2' ;
