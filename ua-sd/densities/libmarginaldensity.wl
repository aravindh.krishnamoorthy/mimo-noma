(* ::Package:: *)

(***
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

File: Marginal Eigenvalue density of an F-distributed Matrix
Author: Aravindh Krishnamoorthy (c) 2020.
E-Mail: aravindh.krishnamoorthy@fau.de

Revision 1: Initial revision for public upload.
***)


BeginPackage["libmarginaldensity`"]


(*** Exported Symbols ***)
(*** Marginal density - g[symbol, m1, m2, q], e.g., g[l,3,3,5] ***)
g
(*** Marginal density (Asymptotic) - ga[symbol, m1, m2, q] - e.g., ga[l,3,3,5] ***)
ga


Begin["`Private`"]


(*** Finite Size ***)
a[i_, j_, m_, n_] = Piecewise[{{i + j - 2, i < m && j < n}, {i + j, i >= m && j >= n}}, i + j - 1]
Z[m1_, m2_, q_, m_, n_] := Piecewise[{{1, q == 1}}, Det[Table[Beta[m2 - q + a[i, j, m, n] + 1, m1 + q - a[i, j, m, n] - 1], {i, 1, q - 1}, {j, 1, q - 1}]]]
gu[m1_, m2_, q_] := Simplify[FunctionExpand[Sum[Sum[(-1)^(m + n)*s^(m + m2 + n - q - 2)*Z[m1, m2, q, m, n], {m, 1, q}], {n, 1, q}]/(s + 1)^(m1 + m2)]]
g[\[Lambda]_, (m1_)?NumericQ, (m2_)?NumericQ, (q_)?NumericQ] := gu[m1, m2, q]/NIntegrate[gu[m1, m2, q], {s, 0, Infinity}] /. s -> \[Lambda]
(*** Asymptotic ***)
ga[\[Lambda]_, (m1_)?NumericQ, (m2_)?NumericQ, (q_)?NumericQ] := Module[{\[Rho]1, \[Rho]2, lb, ub}, \[Rho]1 = q/m1; \[Rho]2 = q/m2; lb = (\[Rho]1/\[Rho]2)*((1 - Sqrt[1 - (1 - \[Rho]1)*(1 - \[Rho]2)])/(1 - \[Rho]1))^2; ub = (\[Rho]1/\[Rho]2)*((Sqrt[1 - (1 - \[Rho]1)*(1 - \[Rho]2)] + 1)/(1 - \[Rho]1))^2; 
     Piecewise[{{((1 - \[Rho]1)*Sqrt[(\[Lambda] - lb)*(ub - \[Lambda])])/(2*Pi*\[Lambda]*(\[Lambda]*\[Rho]1 + \[Rho]1)), \[Lambda] >= lb && \[Lambda] <= ub}}, 0]]


End []


EndPackage[]
