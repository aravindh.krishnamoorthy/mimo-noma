(* ::Package:: *)

(***
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

File: Ordered eigenvalue density of an F-distributed Matrix
Author: Aravindh Krishnamoorthy (c) 2020.
E-Mail: aravindh.krishnamoorthy@fau.de

Revision 1: Initial revision for public upload.
***)


BeginPackage["libordereddensity`"]


(*** Exported Symbol ***)
(*** Ordered density: gl[symbol, eigenvalue_number, m1, m2, q], e.g., gl[l,1,4,4,4] ***)
gl


Begin["`Private`"]


r[i_, n_, q_] := Complement[Range[q], n][[i]]

sg[n_, m_, q_] :=  Product[(-1)^(FirstPosition[Complement[Range[q], m[[1 ;; i - 1]]], m[[i]]] + FirstPosition[Complement[Range[q], n[[1 ;; i - 1]]], n[[i]]]), {i, 1, Length[n]}]

Dmat[l_, m1_, m2_, q_, ns_, ms_] := Piecewise[{{1, l == q}}, Det[Table[s^(m2 - q + r[m, ms, q] + r[n, ns, q] + 1 - 2) \
                                      * Hypergeometric2F1[m1 + m2, m2 - q + r[m, ms, q] + r[n, ns, q] + 1 - 2, m2 - q + r[m, ms, q] + r[n, ns, q] + 2 - 2, -s]/(m2 - q + r[m, ms, q] + r[n, ns, q] + 1 - 2), {n, 1, q - l}, {m, 1, q - l}]]]

glpdf[l_, m1_, m2_, q_, d_, ns_, ms_] := Sum[glpdf[l, m1, m2, q, d - 1, Append[ns, n], Append[ms, m]], {n, Complement[Range[q], ns]}, {m, Complement[Range[q], ms]}]

glpdf[l_, m1_, m2_, q_] := glpdf[l, m1, m2, q, l, {}, {}]

glpdf[l_, m1_, m2_, q_, 1, ns_, ms_] := Product[(-1)^(ms[[i]] + ns[[i]] + 1 - m1 - q) FunctionExpand[Beta[-1/s, 1 + m1 - ms[[i]] - ns[[i]] + q, 1 - m1 - m2]], {i, 1, l - 1}]*Sum[(s^(m + m2 + n - q - 2)*sg[Append[ns, n], Append[ms, m], q] \
                                          * Dmat[l, m1, m2, q, Append[ns, n], Append[ms, m]])/(s + 1)^(m1 + m2), {n, Complement[Range[q], ns]}, {m, Complement[Range[q], ms]}]

glu[l_, m1_, m2_, q_] := Simplify[glpdf[l, m1, m2, q], s \[Element] Reals]

gl[\[Lambda]_, l_?NumericQ, m1_?NumericQ, m2_?NumericQ, q_?NumericQ] := (glu[l, m1, m2, q]/NIntegrate[glu[l, m1, m2, q], {s, 0, Infinity}]) /. s->\[Lambda]


End []


EndPackage[]
