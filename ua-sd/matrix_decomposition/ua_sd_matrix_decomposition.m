% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
% File: SD Matrix Decomposition
% Author: Aravindh Krishnamoorthy (c) 2020.
% E-Mail: aravindh.krishnamoorthy@fau.de
%
% Revision 1: Initial revision for public upload.
%

function [Q1,Q2,P] = ua_sd_matrix_decomposition(H1,H2)

% Obtain various parameters from H1 and H2
[M1,N] = size(H1) ;
[M2,NP] = size(H2) ;
MB1 = min(M1,max(0,N-M2)) ;
MB2 = min(M1,max(0,N-M1)) ;
M = max(0,N-MB1-MB2) ;

% Checks
assert(N == NP) ;
assert (M1 <= N && M2 <= N) ;

if M1+M2 <= N
    R1 = null(H1) ;
    R1 = R1(:,1:MB1) ;
    R2 = null(H2) ;
    R2 = R2(:,1:MB2) ;
    [U1,~,V1] = svd(H1*R2) ;
    [U2,~,V2] = svd(H2*R1) ;
    Q1 = U1' ;
    Q2 = U2' ;
    P = [R2*V1/sqrt(MB1) R1*V2/sqrt(MB2)] ;
else
    
% Subspaces
K = null([null(H1),null(H2)]') ;
R1 = [K,null(H2)] ;
R2 = null(H1) ;

% First precoder - part 1
[U1,S1,V1] = svd(H1*R1) ;
Q11 = U1' ;
P1 = R1*V1*pinv(S1) ;
% Part 2
[U2,~,V2] = svd(H2*R2) ;
% Part 3
[Q,R] = qr((U2'*H2*P1)') ;
if norm(R) < 10e-6
    R = zeros(size(R)) ;
    Q = eye(size(Q)) ;
end
B1 = R(1:M,MB2+1:end)' ;
[U,SB,V] = svd(B1) ;
SB = SB(1:M,1:M) ;
U = blkdiag(eye(MB2),U) ;
V = blkdiag(V,eye(MB1),diag(diag(zeros(M1-M-MB1,1)))) ;
% Part 4
Q1 = V'*Q'*Q11 ;
P2 = Q ;
Q2 = U'*U2' ;

%% Second precoder - part - 1
PP = P1*P2*V*blkdiag(diag(1./sqrt(1+diag(SB).^2)),eye(MB1),diag(diag(zeros(M1-M-MB1,1)))) ;
%% Part 2
PS = R2*V2/sqrt(MB2) ;

% Combined precoder
P = [PP PS] ;
end
