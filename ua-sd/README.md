# Precoder Design and Statistical Power Allocation for MIMO-NOMA via User-Assisted Simultaneous Diagonalization

| Directory | Relevant Paper |
| --------- | ----- |
| densities | Finite-regime marginal and ordered eigenvalue densities for an F-distributed matrix derived in Theorems 2 and 4. |
| matrix_decomposition | UA-SD matrix decomposition proposed in Theorem 1. |

