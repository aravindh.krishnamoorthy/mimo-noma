# MIMO-NOMA

Public code corresponding to MIMO-NOMA research at Institute for Digital Communications, Friedrich-Alexander-Universität Erlangen-Nürnberg.

Website: [https://sites.google.com/view/aravindhkrishnamoorthy](https://sites.google.com/view/aravindhkrishnamoorthy)

Further details are available in the individual README.md files in the directories below.

## Contents
| Directory | Id | Relevant Paper |
| --------- | -- | ----- |
| ua-sd | [arXiv:2005.02308](https://arxiv.org/abs/2005.02308), [IEEE](https://ieeexplore.ieee.org/abstract/document/9250605) | Precoder Design and Statistical Power Allocation for MIMO-NOMA via User-Assisted Simultaneous Diagonalization |
| st | [arXiv:2006.06471](https://arxiv.org/abs/2006.06471), [IEEE](https://ieeexplore.ieee.org/abstract/document/9321706) | Uplink and downlink MIMO-NOMA with simultaneous triangularization|

# Submissions
Submission of open source equivalents or updates to the provided code are welcome.

# Bugs and Questions
Bug reports can be filed either on GitLab or by e-mail: aravindh.krishnamoorthy@fau.de

# Updates
This website will be continually updated to incorporate additional resources, bugfixes, and open source equivalents of any proprietary code.
